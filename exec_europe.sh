#!/bin/bash

LOGFILE=/home/big/task.log
echo "`date +%H:%M:%S` : Starting work" >> $LOGFILE

declare -a country

country=(fr de uk pt it bg ch)


location_file="/home/big/openpyn-nordvpn/europe_location.txt"


for i in ${country[@]}
do 
	echo $i
	# write $country_location.csv file

	file="/home/big/openpyn-nordvpn/"$i"_location.csv"
	echo $file
	if [ ! -f "$file" ]; then
		openpyn -l $i | tee "$i"_location.csv
		
	fi 
done


if [ ! -f "$location_file" ]; then
	touch $location_file
	
else
	truncate -s 0 $location_file
fi


for i in ${country[@]}
do 
	file="/home/big/openpyn-nordvpn/"$i"_location.csv"
	IFS=","

	while read f1 f2 f3 f4 f5
	do
	a=$(echo "$f1" | cut -d"=" -f2)

	
	if [[ "$a" == *"$i"* ]] && [[ "$a" != *"'$i"* ]];
	then	
		
		loc_arr+=(\"${a//[[:space:]]}\")
		echo "$a" >> europe_location.txt
			
	fi
	done < "$file"
done


csv="/home/big/web_crawler/comp42_10001.csv"
csv2="/home/big/web_crawler/result_comp43.csv"

count1=$(wc -l "$csv" | cut -d' ' -f1)
count2=$(wc -l "$csv2" | cut -d' ' -f1)
echo $count1
echo $count2

while [ "$count2" -le "$count1" ] 
do
	echo "pass"
	rand=$(shuf -n 1  europe_location.txt)
	echo $rand
	timeout 50 openpyn -s ${rand//[[:blank:]]/}
	openpyn -k

done

echo "`date +%H:%M:%S` : End work" >> $LOGFILE

